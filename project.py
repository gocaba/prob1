# -*- coding: utf-8 -*-
"""
Created on Sat Dec 12 18:26:34 2020

@author: Gonzalo Cabanillas
"""
import sys

import csv

from common import test_env
from common import describe_data
from common import classification_metrics
from plotly.colors import n_colors

import numpy as np
import pandas as pd

import plotly.express as px
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import MultinomialNB
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.compose import make_column_transformer
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import LabelEncoder
from sklearn.pipeline import make_pipeline
from sklearn.svm import SVR
from sklearn.metrics import mean_absolute_error,mean_squared_error
from sklearn.tree import DecisionTreeRegressor

def read_data(file):
    """Return pandas dataFrame read from CSV file"""
    try:
        return csv.DictReader(file)
    except FileNotFoundError:
        sys.exit('ERROR: ' + file + ' not found')


def linear_regression(X, y, print_text='Linear regression all in'):
    # Split train test sets
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.25, random_state=0)
    # Linear regression all in
    reg = LinearRegression()
    reg.fit(X_train, y_train)
    classification_metrics.print_metrics(y_test, reg.predict(X_test), print_text)
    return reg

def Logistic_Regression(df, y):
    X_train, X_test, y_train, y_test = train_test_split(
        df, y, test_size=0.25, random_state=0)

    sc = StandardScaler()
    X_train = sc.fit_transform(X_train)
    X_test = sc.transform(X_test)

    clf = LogisticRegression(random_state=0, solver='lbfgs')
    clf.fit(X_train, y_train)
    y_pred = clf.predict(X_test)

    return classification_metrics.print_metrics(y_test, y_pred, 'Logistic Regression')


if __name__ == '__main__':
    modules = ['numpy', 'pandas', 'sklearn']
    test_env.versions(modules)
    # Read from the file and load it
    avocado = pd.read_csv('avocado.csv')
    # I think that having the overview of the data can be a positive thing
    describe_data.print_overview(
        avocado, file='results/avocado_prices_overview.txt')
    
    describe_data.print_categorical(
        avocado, file='results/avocado_prices_categorical_data.txt')
    # Any null values?
    avocado.info()
    # Drop first column, useless
    avocado.drop('Unnamed: 0', axis=1, inplace=True)
    
    # Need to get int in order to make the graphs easily
    need_to_convert = ['Total Volume', 
              '4046', 
              '4225', 
              '4770', 
              'Total Bags', 
              'Small Bags', 
              'Large Bags', 
              'XLarge Bags']
    for i in need_to_convert:
        avocado[i] = avocado[i].astype('int64')
    
    # Date_form
    avocado['Date'] = pd.to_datetime(avocado['Date'])
    
    #AveragePrice per Region:
    plt.figure(figsize = (20,6))
    chart = sns.barplot(data = avocado, x = 'region', y = 'AveragePrice')
    chart.set_xticklabels(chart.get_xticklabels(), rotation=45,horizontalalignment='right')
    
    # Average Price per Year:
    plt.figure(figsize = (4,6))
    chart = sns.barplot(data = avocado, x = 'year', y = 'AveragePrice')
    chart.set_xticklabels(chart.get_xticklabels(), rotation=45,horizontalalignment='right')

    # Average Price per Type:
    plt.figure(figsize = (2,20))
    chart = sns.barplot(data = avocado, x = 'type', y = 'AveragePrice')
    chart.set_xticklabels(chart.get_xticklabels(), rotation=45,horizontalalignment='right')
    
    # *****************Plot correlation with scatter plot****************
    figure_3 = 'SME AveragePrice and Date correlation conventional and organic avocados'
    sub_avo=avocado.query("type == 'conventional'")
    plt.figure(figure_3)
    plt.suptitle(figure_3)
    plt.scatter(sub_avo.AveragePrice,sub_avo.Date, s=2, color='green')
    plt.xlabel('AveragePrice')
    plt.ylabel('Date')
    # plt.show()
    sub_avo1=avocado.query("type == 'organic'")
    # plt.figure(figure_4)
    # plt.suptitle(figure_4)
    plt.scatter(sub_avo1.AveragePrice,sub_avo1.Date, s=2, color='red')
    plt.xlabel('AveragePrice')
    plt.ylabel('Date')
    plt.show()
    
    plt.figure(4)
    pd.plotting.scatter_matrix(sub_avo,figsize=(25,25))
    plt.show()
    # ***********************************************************
    
    # *****************Plot correlation with scatter plot****************
    
    plt.figure(5)
    f,ax=plt.subplots(figsize=(20,20))

    # sns.heatmap(avocado.corr(),annot=True,fmt='.2f',ax=ax,vmin=-1, vmax=1, center= 0, cmap= 'coolwarm',linewidths=3, linecolor='black')
    
    
    avocado2= avocado.copy()
    
    avocado2['Date_X'] = avocado2['Date'].apply(lambda x: x.quarter)
    avocado2['Date_X'].value_counts()
    columns = ['AveragePrice', 'Total Volume', '4046', '4225', '4770', 'Total Bags', 'Small Bags', 'Large Bags', 'XLarge Bags', 
           'year', 'Date_X']
    avo = np.corrcoef(avocado2[columns].values.T)
    sns.set(font_scale = 1.8)
    sns.heatmap(avo,cbar = True, annot = True,square = True, fmt = '.2f', annot_kws = {'size':15}, yticklabels = columns, 
                     xticklabels = columns)
    plt.show()
    # ***********************************************************
    
    avocado1= avocado.copy()
    avocado5= avocado.copy()
    
    avocado5=avocado5.astype({'AveragePrice':'int'})
    avocado1.drop(columns=['Date'], inplace=True)
    avocado1.drop(columns=['type'], inplace=True)
    avocado1.drop(columns=['region'], inplace=True)
    avocado5.drop(columns=['Date'], inplace=True)
    
    avocado1['idx'] = [ i for i in range(len(avocado1)) ]
    # ***********************************************************
   
    print('*************************************')
    
    # First regression
    # ***********************REGRESSION 1 ***********************
    X1 = np.array(avocado1["idx"]).reshape(-1, 1)
    y1 = avocado1['AveragePrice']
    
    X_train, X_test, y_train, y_test = train_test_split(X1, y1, test_size=0.25)
    clf1 = LinearRegression(fit_intercept=True)
    clf1.fit(X_train, y_train)
    LinearRegression(copy_X=True, fit_intercept=True, n_jobs=None, normalize=False)
    y_pred = clf1.predict(X_test)
    score = mean_squared_error(y_test, y_pred)
    print('Linear regression score 1: ', score)
    plt.figure(6)
    plt.title('Avocado Trending Prices')
    plt.xlabel('Time')
    plt.ylabel('Prices')
    plt.scatter(X_test, y_test, color='red',s=1)
    plt.plot(X_test, y_pred, color='blue')
    plt.xticks(())
    plt.yticks(())
    print('MAE for testing set: {}'.format(mean_absolute_error(y_pred,y_test)))
    print('MSE for testing set: {}'.format(mean_squared_error(y_pred,y_test)))
    print('RMSE for testing set: {}'.format(np.sqrt(mean_squared_error(y_pred,y_test))))
    plt.show()
    # ***********************************************************
    
    # Second regression
    # ***********************REGRESSION 2 ***********************
    X2 = np.array(avocado1["idx"]).reshape(-1, 1)
    y2 = avocado1['Total Volume']
    
    X_train2, X_test2, y_train2, y_test2 = train_test_split(X2, y2, test_size=0.25)
    clf2 = LinearRegression(fit_intercept=True)
    clf2.fit(X_train2, y_train2)
    LinearRegression(copy_X=True, fit_intercept=True, n_jobs=None, normalize=False)
    y_pred2 = clf2.predict(X_test2)
    score2 = mean_squared_error(y_test2, y_pred2)
    plt.figure(7)
    plt.title('Avocado Volume evolution')
    plt.xlabel('Time')
    plt.ylabel('Total Volume')
    plt.scatter(X_test2, y_test2, color='red',s=1)
    plt.plot(X_test2, y_pred2, color='blue')
    plt.xticks(())
    plt.yticks(())
    plt.show()
    # ***********************************************************
    
    # Third regression
    # ***********************REGRESSION 3 ***********************
    X3 = np.array(avocado1['Total Bags']).reshape(-1, 1)
    y3 = avocado1['Small Bags']
    
    X_train3, X_test3, y_train3, y_test3 = train_test_split(X3, y3, test_size=0.25)
    clf3 = LinearRegression(fit_intercept=True)
    clf3.fit(X_train3, y_train3)
    LinearRegression(copy_X=True, fit_intercept=True, n_jobs=None, normalize=False)
    y_pred3 = clf3.predict(X_test3)
    score3 = mean_squared_error(y_test3, y_pred3)
    plt.figure(7)
    plt.title('Avocado Samll Bags evolution')
    plt.xlabel('Total Bags')
    plt.ylabel('Small Bags')
    plt.scatter(X_test3, y_test3, color='red',s=1)
    plt.plot(X_test3, y_pred3, color='blue')
    plt.xticks(())
    plt.yticks(())
    plt.show()
    # ***********************************************************
    
    print('*************************************')
    
    # ***********************REGRESSION SVR ***********************
    label_cols = ['type','region']
    label = LabelEncoder()
    avocado5[label_cols] = avocado5[label_cols].apply(lambda x : label.fit_transform(x))
    scale_cols = avocado5.drop(['AveragePrice','type','year','region'],axis=1)
    col_trans = make_column_transformer(
            (OneHotEncoder(), avocado5[label_cols].columns),
            (StandardScaler(), scale_cols.columns),
            remainder = 'passthrough')
    X = avocado5.drop(['AveragePrice'],axis=1)
    y = avocado5.AveragePrice
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.20, random_state = 0)
    linreg = LinearRegression()
    pipe = make_pipeline(col_trans,linreg)
    pipe.fit(X_train,y_train)
    svr = SVR()
    pipe = make_pipeline(col_trans,svr)
    pipe.fit(X_train,y_train)
    y_pred_test = pipe.predict(X_test)
    print('SVR regression score')
    print('MAE for testing set: {}'.format(mean_absolute_error(y_pred_test,y_test)))
    print('MSE for testing set: {}'.format(mean_squared_error(y_pred_test,y_test)))
    print('RMSE for testing set: {}'.format(np.sqrt(mean_squared_error(y_pred_test,y_test))))
    # ***********************************************************

    print('*************************************')

    # ***********************REGRESSION Tree ***********************
        
    dr=DecisionTreeRegressor()
    pipe = make_pipeline(col_trans,dr)
    pipe.fit(X_train,y_train)
    y_pred_test = pipe.predict(X_test)
    print('Tree regression score')
    print('MAE for testing set: {}'.format(mean_absolute_error(y_pred_test,y_test)))
    print('MSE for testing set: {}'.format(mean_squared_error(y_pred_test,y_test)))
    print('RMSE for testing set: {}'.format(np.sqrt(mean_squared_error(y_pred_test,y_test))))
    # ***********************************************************
    print('End of program.')