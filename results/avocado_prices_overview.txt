## Data frame info:
<class 'pandas.core.frame.DataFrame'>
RangeIndex: 18249 entries, 0 to 18248
Data columns (total 14 columns):
 #   Column        Non-Null Count  Dtype  
---  ------        --------------  -----  
 0   Unnamed: 0    18249 non-null  int64  
 1   Date          18249 non-null  object 
 2   AveragePrice  18249 non-null  float64
 3   Total Volume  18249 non-null  float64
 4   4046          18249 non-null  float64
 5   4225          18249 non-null  float64
 6   4770          18249 non-null  float64
 7   Total Bags    18249 non-null  float64
 8   Small Bags    18249 non-null  float64
 9   Large Bags    18249 non-null  float64
 10  XLarge Bags   18249 non-null  float64
 11  type          18249 non-null  object 
 12  year          18249 non-null  int64  
 13  region        18249 non-null  object 
dtypes: float64(9), int64(2), object(3)
memory usage: 1.9+ MB
None


## Data frame shape:
18249 rows
14 columns


## Data frame columns:
Unnamed: 0
Date
AveragePrice
Total Volume
4046
4225
4770
Total Bags
Small Bags
Large Bags
XLarge Bags
type
year
region


## Data head and tail:
   Unnamed: 0        Date  AveragePrice  Total Volume        4046          4225       4770   Total Bags   Small Bags  Large Bags  XLarge Bags          type  year  region
0           0  2015-12-27      1.330000  64236.620000 1036.740000  54454.850000  48.160000  8696.870000  8603.620000   93.250000     0.000000  conventional  2015  Albany
1           1  2015-12-20      1.350000  54876.980000  674.280000  44638.810000  58.330000  9505.560000  9408.070000   97.490000     0.000000  conventional  2015  Albany
2           2  2015-12-13      0.930000 118220.220000  794.700000 109149.670000 130.500000  8145.350000  8042.210000  103.140000     0.000000  conventional  2015  Albany
3           3  2015-12-06      1.080000  78992.150000 1132.000000  71976.410000  72.580000  5811.160000  5677.400000  133.760000     0.000000  conventional  2015  Albany
4           4  2015-11-29      1.280000  51039.600000  941.480000  43838.390000  75.780000  6183.950000  5986.260000  197.690000     0.000000  conventional  2015  Albany
5           5  2015-11-22      1.260000  55979.780000 1184.270000  48067.990000  43.610000  6683.910000  6556.470000  127.440000     0.000000  conventional  2015  Albany
6           6  2015-11-15      0.990000  83453.760000 1368.920000  73672.720000  93.260000  8318.860000  8196.810000  122.050000     0.000000  conventional  2015  Albany
7           7  2015-11-08      0.980000 109428.330000  703.750000 101815.360000  80.000000  6829.220000  6266.850000  562.370000     0.000000  conventional  2015  Albany
8           8  2015-11-01      1.020000  99811.420000 1022.150000  87315.570000  85.340000 11388.360000 11104.530000  283.830000     0.000000  conventional  2015  Albany
9           9  2015-10-25      1.070000  74338.760000  842.400000  64757.440000 113.000000  8625.920000  8061.470000  564.450000     0.000000  conventional  2015  Albany
...
       Unnamed: 0        Date  AveragePrice  Total Volume        4046        4225       4770   Total Bags   Small Bags  Large Bags  XLarge Bags     type  year            region
18244           7  2018-02-04      1.630000  17074.830000 2046.960000 1529.200000   0.000000 13498.670000 13066.820000  431.850000     0.000000  organic  2018  WestTexNewMexico
18245           8  2018-01-28      1.710000  13888.040000 1191.700000 3431.500000   0.000000  9264.840000  8940.040000  324.800000     0.000000  organic  2018  WestTexNewMexico
18246           9  2018-01-21      1.870000  13766.760000 1191.920000 2452.790000 727.940000  9394.110000  9351.800000   42.310000     0.000000  organic  2018  WestTexNewMexico
18247          10  2018-01-14      1.930000  16205.220000 1527.630000 2981.040000 727.010000 10969.540000 10919.540000   50.000000     0.000000  organic  2018  WestTexNewMexico
18248          11  2018-01-07      1.620000  17489.580000 2894.770000 2356.130000 224.530000 12014.150000 11988.140000   26.010000     0.000000  organic  2018  WestTexNewMexico


## Numeric values statistics:
         Unnamed: 0        Date  AveragePrice    Total Volume            4046            4225           4770      Total Bags      Small Bags     Large Bags   XLarge Bags          type         year       region
count  18249.000000       18249  18249.000000    18249.000000    18249.000000    18249.000000   18249.000000    18249.000000    18249.000000   18249.000000  18249.000000         18249 18249.000000        18249
unique          nan         169           nan             nan             nan             nan            nan             nan             nan            nan           nan             2          nan           54
top             nan  2016-11-20           nan             nan             nan             nan            nan             nan             nan            nan           nan  conventional          nan  GrandRapids
freq            nan         108           nan             nan             nan             nan            nan             nan             nan            nan           nan          9126          nan          338
mean      24.232232         NaN      1.405978   850644.013009   293008.424531   295154.568356   22839.735993   239639.202060   182194.686696   54338.088145   3106.426507           NaN  2016.147899          NaN
std       15.481045         NaN      0.402677  3453545.355399  1264989.081763  1204120.401135  107464.068435   986242.399216   746178.514962  243965.964547  17692.894652           NaN     0.939938          NaN
min        0.000000         NaN      0.440000       84.560000        0.000000        0.000000       0.000000        0.000000        0.000000       0.000000      0.000000           NaN  2015.000000          NaN
25%       10.000000         NaN      1.100000    10838.580000      854.070000     3008.780000       0.000000     5088.640000     2849.420000     127.470000      0.000000           NaN  2015.000000          NaN
50%       24.000000         NaN      1.370000   107376.760000     8645.300000    29061.020000     184.990000    39743.830000    26362.820000    2647.710000      0.000000           NaN  2016.000000          NaN
75%       38.000000         NaN      1.660000   432962.290000   111020.200000   150206.860000    6243.420000   110783.370000    83337.670000   22029.250000    132.500000           NaN  2017.000000          NaN
max       52.000000         NaN      3.250000 62505646.520000 22743616.170000 20470572.610000 2546439.110000 19373134.370000 13384586.800000 5719096.610000 551693.650000           NaN  2018.000000          NaN


